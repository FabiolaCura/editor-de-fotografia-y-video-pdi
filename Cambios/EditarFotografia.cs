﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Procesamiento_de_imagenes.Cambios
{
    public partial class EditarFotografia : Form
    {
        #region Variables_Privadas

        private int[] Ahistograma01 = new int[256];
        private int[] Ahistograma02 = new int[256];
        private int[] Ahistograma03 = new int[256];

        private int mayor1;
        private int mayor2;
        private int mayor3;

        private Bitmap original;
        private Bitmap resultante;
        private Bitmap copia;
        private Bitmap original2;

        private int[,] mascara = new int[3, 3];

        #endregion

        static public bool Im = true;

        PaintEventArgs H1;

        public EditarFotografia()
        {
            InitializeComponent();
            if (Form1.enviandoFoto == true)
            {
                Form1.enviandoFoto = false;
                original = (Bitmap)Form1.fotoEnviada;
                resultante = (Bitmap)Form1.fotoEnviada;
                PB_ImEdit.Image = new Bitmap(original);
                using (var dialogo = new SplashS(AcHistograma))
                {
                    dialogo.ShowDialog(this);
                }
                Histograma01.Refresh();
                Histograma02.Refresh();
                Histograma03.Refresh();
            }
        }

        #region Guardar_Cargar
        private void SeleccImagen_Click(object sender, EventArgs e)
        {
            resultante = original = null;
            //OF_SeleccImagen.InitialDirectory = "C:\\";
            OF_SeleccImagen.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            OF_SeleccImagen.Filter = "All files (*.*)|*.* | Image Files(*.BMP;*.JPG; *.PNG;*.GIF)|*.BMP;*.JPG; *.PNG*;.GIF";
            OF_SeleccImagen.FilterIndex = 2;
            OF_SeleccImagen.RestoreDirectory = true;
            if (OF_SeleccImagen.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    // PB_ImEdit.Image = Image.FromFile(OF_SeleccImagen.FileName);
                    PB_ImEdit.Image = new Bitmap(OF_SeleccImagen.FileName);
                    Im = false;
                    original = new Bitmap(PB_ImEdit.Image);

                    using (var dialogo = new SplashS(AcHistograma))
                    {
                        dialogo.ShowDialog(this);
                    }
                    Histograma01.Refresh();
                    Histograma02.Refresh();
                    Histograma03.Refresh();
                }
                catch (Exception)
                {
                    Im = true;
                    throw;
                }
            }

        }

        private void Guardar_Click(object sender, EventArgs e)
        {
            SFD_GuardarImagen.CheckFileExists = false;
            SFD_GuardarImagen.Filter = "All files (*.*)|*.* | Image Files(*.BMP;*.JPG; *.PNG;*.GIF)|*.BMP;*.JPG; *.PNG*;.GIF";
            SFD_GuardarImagen.FileName = "Imagen";
            SFD_GuardarImagen.FilterIndex = 2;
            SFD_GuardarImagen.DefaultExt = "jpg";

            if (SFD_GuardarImagen.ShowDialog() == DialogResult.OK)
            {
                PB_ImEdit.Image.Save(SFD_GuardarImagen.FileName);
                Im = true;
                DialogResult resultado = new DialogResult();
                Form mensaje = new MessageBoxImagenGuardada();
                resultado = mensaje.ShowDialog();

            }
        }

        #endregion

        private void escalaraGrises()
        {
            resultante = new Bitmap(PB_ImEdit.Image);
            for (int i = 0; i < resultante.Width; i++)
            {
                for (int j = 0; j < resultante.Height; j++)
                {
                    Color imgPixel = resultante.GetPixel(i, j);

                    int rojo = imgPixel.R;
                    int verde = imgPixel.G;
                    int azul = imgPixel.B;

                    //Grises dividir entre 3
                    int pixel = (rojo + verde + azul) / 3;

                    resultante.SetPixel(i, j, Color.FromArgb(pixel, pixel, pixel));
                }
            }
        }
        private void btn_Grises_Click(object sender, EventArgs e)
        {
            if (original != null)
            {
                using (var dialogo = new SplashS(escalaraGrises))
                {
                    dialogo.ShowDialog(this);
                }
                PB_ImEdit.Image = resultante;
                using (var dialogo = new SplashS(AcHistograma))
                {
                    dialogo.ShowDialog(this);
                }
                Histograma01.Refresh();
                Histograma02.Refresh();
                Histograma03.Refresh();
            }
        }
        private void Original_Click(object sender, EventArgs e)
        {
            if (original != null)
            {
                PB_ImEdit.Image = original;
                using (var dialogo = new SplashS(AcHistograma))
                {
                    dialogo.ShowDialog(this);
                }
                Histograma01.Refresh();
                Histograma02.Refresh();
                Histograma03.Refresh();
            }
        }

        private void Laplaciano_Click(object sender, EventArgs e)
        {
            if (original != null)
            {
                using (var dialogo = new SplashS(CalculoLaplaciano))
                {
                    dialogo.ShowDialog(this);
                }
                using (var dialogo = new SplashS(AcHistograma))
                {
                    dialogo.ShowDialog(this);
                }
                Histograma01.Refresh();
                Histograma02.Refresh();
                Histograma03.Refresh();
            }

        }
        private void Prewitt_Click(object sender, EventArgs e)
        {
            if (original != null)
            {
                using (var dialogo = new SplashS(CalculoPrewitt))
                {
                    dialogo.ShowDialog(this);
                }
                using (var dialogo = new SplashS(AcHistograma))
                {
                    dialogo.ShowDialog(this);
                }
                Histograma01.Refresh();
                Histograma02.Refresh();
                Histograma03.Refresh();
            }
        }

        private void CalculoLaplaciano()
        {
            //mascara = new int[,] {
            //                     {0, 1, 0},
            //                     {1, -4, 1},
            //                     {0, 1, 0} };
            mascara = new int[,] {
                                 {-1, -1, -1},
                                 {-1, 8, -1},
                                 {-1, -1, -1} };
            escalaraGrises();
            copia = (Bitmap)resultante.Clone();
            CalculoConMatrices(0, 255);
            PB_ImEdit.Image = resultante;
        }
        private void CalculoPrewitt()
        {
            //mascara = new int[,] {
            //                     {-1, 0, 1},
            //                     {-1, 0, 1},
            //                     {-1, 0, 1} };
            mascara = new int[,] {
                                 {-1, -1, -1},
                                 { 0,  0,  0},
                                 { 1,  1,  1} };
            escalaraGrises();
            copia = (Bitmap)resultante.Clone();
            CalculoConMatrices(0, 255);
            PB_ImEdit.Image = resultante;
        }

        #region Histograma


        private void Histograma02_Paint(object sender, PaintEventArgs e)
        {
            int n = 20;
            int altura = 0;
            Graphics g = e.Graphics;
            Pen plumaH = new Pen(Color.Green);

            for (n = 0; n < 256; n++)
            {
                g.DrawLine(plumaH, n + 20, 270, n + 20, 270 - Ahistograma02[n]);
            }
        }

        private void Histograma03_Paint(object sender, PaintEventArgs e)
        {
            int n = 20;
            int altura = 0;
            Graphics g = e.Graphics;
            Pen plumaH = new Pen(Color.Blue);

            for (n = 0; n < 256; n++)
            {
                g.DrawLine(plumaH, n + 20, 270, n + 20, 270 - Ahistograma03[n]);
            }
        }

        private void Histograma01_Paint_1(object sender, PaintEventArgs e)
        {
            H1 = e;
            int n = 0;
            int altura = 0;
            Graphics g = e.Graphics;
            Pen plumaH = new Pen(Color.Red);

            for (n = 0; n < 256; n++)
            {
                g.DrawLine(plumaH, n + 20, 270, n + 20, 270 - Ahistograma01[n]);
            }
        }

        private void AcHistograma()
        {
            resultante = new Bitmap(PB_ImEdit.Image);
            int w = original.Width;
            int h = original.Height;
            int x = 0;
            int y = 0;

            Color RGB = new Color();

            for (x = 0; x < w; x++)
            {
                for (y = 0; y < h; y++)
                {
                    RGB = resultante.GetPixel(x, y);
                    Ahistograma01[RGB.R]++;
                    Ahistograma02[RGB.G]++;
                    Ahistograma03[RGB.B]++;
                }
            }


            int n = 0;
            mayor1 = 0;
            mayor2 = 0;
            mayor3 = 0;
            for (n = 0; n < 256; n++)
            {
                if (Ahistograma01[n] > mayor1)
                    mayor1 = Ahistograma01[n];
                if (Ahistograma02[n] > mayor2)
                    mayor2 = Ahistograma02[n];
                if (Ahistograma03[n] > mayor3)
                    mayor3 = Ahistograma03[n];
            }
            for (n = 0; n < 256; n++)
            {
                Ahistograma01[n] = (int)((float)Ahistograma01[n] / (float)mayor1 * 256.0f);
                Ahistograma02[n] = (int)((float)Ahistograma02[n] / (float)mayor2 * 256.0f);
                Ahistograma03[n] = (int)((float)Ahistograma03[n] / (float)mayor3 * 256.0f);
            }

        }

        #endregion

        private void CalculoConMatrices(int pInferior, int pSuperior)
        {
            Color oColor;

            int suma = 0;

            for (int x = 1; x < copia.Width - 1; x++)
            {
                for (int y = 1; y < copia.Height - 1; y++)
                {
                    suma = 0;
                    for (int a = -1; a < 2; a++)
                    {
                        for (int b = -1; b < 2; b++)
                        {
                            oColor = copia.GetPixel(x + a, y + b);
                            suma = suma + (oColor.R * mascara[a + 1, b + 1]);
                        }
                    }

                    if (suma < pInferior)
                        suma = 0;
                    else if (suma > pSuperior)
                        suma = 255;

                    resultante.SetPixel(x, y, Color.FromArgb(suma, suma, suma));
                }
            }

        }
        private void SegundaImagen()
        {
            original2 = null;
            OF_SeleccImagen.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            OF_SeleccImagen.Filter = "All files (*.*)|*.* | Image Files(*.BMP;*.JPG; *.PNG;*.GIF)|*.BMP;*.JPG; *.PNG*;.GIF";
            OF_SeleccImagen.FilterIndex = 2;
            OF_SeleccImagen.RestoreDirectory = true;
            if (OF_SeleccImagen.ShowDialog() == DialogResult.OK)
            {
                try
                {

                    original2 = new Bitmap(OF_SeleccImagen.FileName);
                    if (original2 != null)
                    {
                        RedimensionarImagen();
                    }
                }
                catch (Exception)
                {
                    DialogResult resultado = new DialogResult();
                    Form mensaje = new MessageBoxError();
                    resultado = mensaje.ShowDialog();
                }
            }
        }
        private void RedimensionarImagen()
        {
            //Saber si es vertical u horizontal
            var Radio = Math.Max((double)original.Width / original2.Width, (double)original.Height / original2.Height);
            var Ancho = (int)(original2.Width * Radio);
            var Alto = (int)(original2.Height * Radio);
            var ImagenRedimencionada = new Bitmap(Ancho, Alto);
            Graphics.FromImage(ImagenRedimencionada).DrawImage(original2, 0, 0, Ancho, Alto);
            original2 = new Bitmap(ImagenRedimencionada);
        }

        private void btn_Adicion_Click(object sender, EventArgs e)
        {
            if (original != null)
            {
                resultante = new Bitmap(PB_ImEdit.Image);
                SegundaImagen();
                using (var dialogo = new SplashS(Adicion))
                {
                    dialogo.ShowDialog(this);
                }
                PB_ImEdit.Image = resultante;
                if(original2 != null)
                {
                    using (var dialogo = new SplashS(AcHistograma))
                    {
                        dialogo.ShowDialog(this);
                    }
                    Histograma01.Refresh();
                    Histograma02.Refresh();
                    Histograma03.Refresh();
                }
            }
        }

        private void Adicion()
        {
            if (original2 != null)
            {
                for (int i = 0; i < resultante.Width; i++)
                {
                    for (int j = 0; j < resultante.Height; j++)
                    {
                        Color imgPixel1 = original2.GetPixel(i, j);
                        Color imgPixel2 = resultante.GetPixel(i, j);

                        int rojo1 = imgPixel1.R;
                        int verde1 = imgPixel1.G;
                        int azul1 = imgPixel1.B;

                        int rojo2 = imgPixel2.R;
                        int verde2 = imgPixel2.G;
                        int azul2 = imgPixel2.B;


                        int r = rojo1 + rojo2;
                        int g = verde1 + verde2;
                        int b = azul1 + azul2;

                        while (r > 255)
                        {
                            r = r - 255;
                        }

                        while (g > 255)
                        {
                            g = g - 255;
                        }
                        while (b > 255)
                        {
                            b = b - 255;
                        }
                        resultante.SetPixel(i, j, Color.FromArgb(r, g, b));
                    }
                }
            }
        }
       
        private void btn_and_Click(object sender, EventArgs e)
        {
            if (original != null)
            {
                resultante = new Bitmap(PB_ImEdit.Image);
                SegundaImagen();
                using (var dialogo = new SplashS(AND))
                {
                    dialogo.ShowDialog(this);
                }
                PB_ImEdit.Image = resultante;
                if (original2 != null)
                {
                    using (var dialogo = new SplashS(AcHistograma))
                    {
                        dialogo.ShowDialog(this);
                    }
                    Histograma01.Refresh();
                    Histograma02.Refresh();
                    Histograma03.Refresh();
                }
            }           
            
        }

        private void AND()
        {
            if (original2 != null)
            {
                Color imgPixel1 = new Color();
                Color imgPixel2 = new Color();

                int r = 0;
                int g = 0;
                int b = 0;

                for (int i = 0; i < resultante.Width; i++)
                {
                    for (int j = 0; j < resultante.Height; j++)
                    {
                        imgPixel1 = resultante.GetPixel(i, j);
                        imgPixel2 = original2.GetPixel(i, j);

                        r = imgPixel1.R & imgPixel2.R;
                        g = imgPixel1.G & imgPixel2.G;
                        b = imgPixel1.B & imgPixel2.B;

                        resultante.SetPixel(i, j, Color.FromArgb(r, g, b));
                    }
                }
            }
        }

        private void btn_OR_Click(object sender, EventArgs e)
        {
            if (original != null)
            {
                resultante = new Bitmap(PB_ImEdit.Image);
                SegundaImagen();
                using (var dialogo = new SplashS(OR))
                {
                    dialogo.ShowDialog(this);
                }
                PB_ImEdit.Image = resultante;
                if (original2 != null)
                {
                    using (var dialogo = new SplashS(AcHistograma))
                    {
                        dialogo.ShowDialog(this);
                    }
                    Histograma01.Refresh();
                    Histograma02.Refresh();
                    Histograma03.Refresh();
                }
            }
        }
        private void OR()
        {
            if (original2 != null)
            {
                Color imgPixel1 = new Color();
                Color imgPixel2 = new Color();

                int r = 0;
                int g = 0;
                int b = 0;

                for (int i = 0; i < resultante.Width; i++)
                {
                    for (int j = 0; j < resultante.Height; j++)
                    {
                        imgPixel1 = resultante.GetPixel(i, j);
                        imgPixel2 = original2.GetPixel(i, j);

                        r = imgPixel1.R | imgPixel2.R;
                        g = imgPixel1.G | imgPixel2.G;
                        b = imgPixel1.B | imgPixel2.B;

                        resultante.SetPixel(i, j, Color.FromArgb(r, g, b));
                    }
                }
            }
        }
    }
}
