﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.Cvb;

namespace Procesamiento_de_imagenes.Cambios
{
    public partial class EditarVideo : Form
    {
        VideoCapture capture;
        bool IsPlaying = false;
        int totalFrames;
        int CurrentFrameNo;
        Mat CurrentFrame;
        int FPS;
        bool F1 = false, F2 = false, F3 = false, F4 = false, F5 = false;

        private Bitmap frame;
        private Bitmap imagen;
        private Bitmap imagenOP;
        private Bitmap auxiliar;
        private int[,] mascara = new int[3, 3];

        public EditarVideo()
        {
            InitializeComponent();
        }

        private void SeleccVideo_Click(object sender, EventArgs e)
        {
            OpenFileDialog video = new OpenFileDialog();
            video.Filter = "Video Files(*.MP4;*.3gp; *.MOV;*.MPEG)|*.MP4;*.3gp; *.MOV*;.MPEG";

            if (video.ShowDialog()==DialogResult.OK)
            {
                capture = new VideoCapture(video.FileName);
                totalFrames = Convert.ToInt32(capture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameCount));
                FPS = Convert.ToInt32(capture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.Fps));
                IsPlaying = true;
                CurrentFrame = new Mat();
                CurrentFrameNo = 0;
                capture.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosFrames, CurrentFrameNo);
                capture.Read(CurrentFrame);
                imagen = auxiliar = frame = CurrentFrame.Bitmap;
                PB_Video.Image = frame;
            }

        }

        private void VFiltro01_Click(object sender, EventArgs e)
        {
            if (capture != null)
            {
                F1 = !F1;
            }
            
        }

        private void VFiltro2_Click(object sender, EventArgs e)
        {
            if (capture != null)
            {
                F2 = !F2;
            }
        }

        private void Filtro3_Click(object sender, EventArgs e)
        {
            if (capture != null)
            {
                if (F3 == true)
                {
                    F3 = false;
                }
                else
                {
                    OpenFileDialog ofd = new OpenFileDialog();
                    ofd.Filter = "Image Files(*.BMP;*.JPG; *.PNG;*.GIF)|*.BMP;*.JPG; *.PNG*;.GIF";
                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        imagenOP = new Bitmap(ofd.FileName);
                        if (imagenOP != null)
                        {
                            RedimensionarImagen();
                            F3 = true;
                        }
                        else
                        {
                            F3 = false;

                        }
                    }
                }
            }
        }

        private void Filtro4_Click(object sender, EventArgs e)
        {
            if (capture != null)
            {
                if (F4 == true)
                {
                    F4 = false;
                }
                else
                {
                    OpenFileDialog ofd = new OpenFileDialog();
                    ofd.Filter = "Image Files(*.BMP;*.JPG; *.PNG;*.GIF)|*.BMP;*.JPG; *.PNG*;.GIF";
                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        imagenOP = new Bitmap(ofd.FileName);
                        if (imagenOP != null)
                        {
                            RedimensionarImagen();
                            F4 = true;
                        }
                        else
                        {
                            F4 = false;

                        }
                    }
                }
            }
        }

        private void Filtro5_Click(object sender, EventArgs e)
        {
            if (capture != null)
            {
                if (F5 == true)
                {
                    F5 = false;
                }
                else
                {
                    OpenFileDialog ofd = new OpenFileDialog();
                    ofd.Filter = "Image Files(*.BMP;*.JPG; *.PNG;*.GIF)|*.BMP;*.JPG; *.PNG*;.GIF";
                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        imagenOP = new Bitmap(ofd.FileName);
                        if (imagenOP != null)
                        {
                            RedimensionarImagen();
                            F5 = true;
                        }
                        else
                        {
                            F5 = false;

                        }
                    }
                }
            }
        }       

        private void btn_detener_Click(object sender, EventArgs e)
        {
            if (capture != null && IsPlaying == true)
            {
                IsPlaying = false;
            }
        }

        private void btn_play_Click(object sender, EventArgs e)
        {
            IsPlaying = true;
            PlayVideo();
        }

        private async void PlayVideo()
        {
            while (IsPlaying == true && CurrentFrameNo < totalFrames)
            {
                capture.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosFrames, CurrentFrameNo);
                capture.Read(CurrentFrame);
                frame = CurrentFrame.Bitmap;
                imagen = auxiliar = frame;
                editarFrame();
                PB_Video.Image = imagen;
                CurrentFrameNo += 1;
                //Dividir el segundo por frames
                await Task.Delay(1000 / FPS);
                if (CurrentFrameNo == totalFrames)
                {
                    CurrentFrameNo = 0;
                }
            }

        }

        private void editarFrame()
        {
            if (F1 == true)
            {
                mascara = new int[,] {
                                 {-1, -1, -1},
                                 {-1, 8, -1},
                                 {-1, -1, -1} };
                escalaraGrises();
                auxiliar = (Bitmap)imagen.Clone();
                CalculoConMatrices(0, 255);
            }
            if (F2 == true)
            {
                mascara = new int[,] {
                                 {-1, -1, -1},
                                 { 0,  0,  0},
                                 { 1,  1,  1} };
                escalaraGrises();
                auxiliar = (Bitmap)imagen.Clone();
                CalculoConMatrices(0, 255);
            }
            if (F3 == true)
            {
                if (imagenOP != null)
                {
                    for (int i = 0; i < imagen.Width; i++)
                    {
                        for (int j = 0; j < imagen.Height; j++)
                        {
                            Color imgPixel1 = imagenOP.GetPixel(i, j);
                            Color imgPixel2 = imagen.GetPixel(i, j);

                            int rojo1 = imgPixel1.R;
                            int verde1 = imgPixel1.G;
                            int azul1 = imgPixel1.B;

                            int rojo2 = imgPixel2.R;
                            int verde2 = imgPixel2.G;
                            int azul2 = imgPixel2.B;


                            int r = rojo1 + rojo2;
                            int g = verde1 + verde2;
                            int b = azul1 + azul2;

                            while (r > 255)
                            {
                                r = r - 255;
                            }

                            while (g > 255)
                            {
                                g = g - 255;
                            }
                            while (b > 255)
                            {
                                b = b - 255;
                            }
                            imagen.SetPixel(i, j, Color.FromArgb(r, g, b));
                        }
                    }
                }
            }
            if (F4 == true)
            {
                if (imagenOP != null)
                {
                    Color imgPixel1 = new Color();
                    Color imgPixel2 = new Color();

                    int r = 0;
                    int g = 0;
                    int b = 0;

                    for (int i = 0; i < imagen.Width; i++)
                    {
                        for (int j = 0; j < imagen.Height; j++)
                        {
                            imgPixel1 = imagen.GetPixel(i, j);
                            imgPixel2 = imagenOP.GetPixel(i, j);

                            r = imgPixel1.R & imgPixel2.R;
                            g = imgPixel1.G & imgPixel2.G;
                            b = imgPixel1.B & imgPixel2.B;

                            imagen.SetPixel(i, j, Color.FromArgb(r, g, b));
                        }
                    }
                }
            }
            if (F5 == true)
            {
                if (imagenOP != null)
                {
                    Color imgPixel1 = new Color();
                    Color imgPixel2 = new Color();

                    int r = 0;
                    int g = 0;
                    int b = 0;

                    for (int i = 0; i < imagen.Width; i++)
                    {
                        for (int j = 0; j < imagen.Height; j++)
                        {
                            imgPixel1 = imagen.GetPixel(i, j);
                            imgPixel2 = imagenOP.GetPixel(i, j);

                            r = imgPixel1.R | imgPixel2.R;
                            g = imgPixel1.G | imgPixel2.G;
                            b = imgPixel1.B | imgPixel2.B;

                            imagen.SetPixel(i, j, Color.FromArgb(r, g, b));
                        }
                    }
                }
            }
        }

        private void escalaraGrises()
        {
            for (int i = 0; i < imagen.Width; i++)
            {
                for (int j = 0; j < imagen.Height; j++)
                {
                    Color imgPixel = imagen.GetPixel(i, j);

                    int rojo = imgPixel.R;
                    int verde = imgPixel.G;
                    int azul = imgPixel.B;

                    //Grises dividir entre 3
                    int pixel = (rojo + verde + azul) / 3;

                    imagen.SetPixel(i, j, Color.FromArgb(pixel, pixel, pixel));
                }
            }
        }
        private void CalculoConMatrices(int pInferior, int pSuperior)
        {
            Color oColor;
            int suma = 0;
            for (int x = 1; x < auxiliar.Width - 1; x++)
            {
                for (int y = 1; y < auxiliar.Height - 1; y++)
                {
                    suma = 0;
                    for (int a = -1; a < 2; a++)
                    {
                        for (int b = -1; b < 2; b++)
                        {
                            oColor = auxiliar.GetPixel(x + a, y + b);
                            suma = suma + (oColor.R * mascara[a + 1, b + 1]);
                        }
                    }

                    if (suma < pInferior)
                        suma = 0;
                    else if (suma > pSuperior)
                        suma = 255;

                    imagen.SetPixel(x, y, Color.FromArgb(suma, suma, suma));
                }
            }

        }

        private void RedimensionarImagen()
        {
            //Saber si es vertical u horizontal
            var Radio = Math.Max((double)frame.Width / imagenOP.Width, (double)frame.Height / imagenOP.Height);
            var Ancho = (int)(imagenOP.Width * Radio);
            var Alto = (int)(imagenOP.Height * Radio);
            var ImagenRedimencionada = new Bitmap(Ancho, Alto);
            Graphics.FromImage(ImagenRedimencionada).DrawImage(imagenOP, 0, 0, Ancho, Alto);
            imagenOP = new Bitmap(ImagenRedimencionada);
        }
    }
}
  