﻿using AForge.Video;
using AForge.Video.DirectShow;
using AForge.Vision.Motion;
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace Procesamiento_de_imagenes.Cambios
{
    public partial class TomarFoto : Form
    {
        
        private FilterInfoCollection MisDispositivos;
        private VideoCaptureDevice MiCamara = null;
        static readonly CascadeClassifier cascadeClassifier = new CascadeClassifier("haarcascade_frontalface_alt_tree.xml");
        private int r = 0;
        private int g = 0;
        private int b = 0;
        int cant = 0;
        Colores RGB;
        Bitmap copia;
        public TomarFoto(){
            InitializeComponent();
            RGB = new Colores();
            CargaDeDispositivos();
        }

        public void CargaDeDispositivos()
        {
            MisDispositivos = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            if (MisDispositivos.Count > 0)
            {

                for (int i = 0; i < MisDispositivos.Count; i++)
                {
                    CB_List.Items.Add(MisDispositivos[i].Name.ToString());
                }

                CB_List.Text = MisDispositivos[0].Name.ToString();
            }
        }

        public void CerrarCamara()
        {
            if(MiCamara != null && MiCamara.IsRunning)
            {
                MiCamara.SignalToStop();
                MiCamara = null;
                Form1.CapturandoDispositivo = false;

            }
        }

        private void btn_Grabar_Click(object sender, EventArgs e)
        {
            CerrarCamara();
            Form1.CapturandoDispositivo = true;
            int dispositivoSeleccionado = CB_List.SelectedIndex;
            string nombreDispositivoSelecc = MisDispositivos[dispositivoSeleccionado].MonikerString;
            MiCamara = new VideoCaptureDevice(nombreDispositivoSelecc);
            MiCamara.NewFrame += new NewFrameEventHandler(Capturando);
            MiCamara.Start();
        }

        private void Capturando(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap Imagen = (Bitmap)eventArgs.Frame.Clone();
            copia = (Bitmap)Imagen.Clone();
            Image<Bgr, byte> grayImage = new Image<Bgr, byte>(Imagen);
            Rectangle[] rectangles = cascadeClassifier.DetectMultiScale(grayImage, 1.2, 1);
            cant = rectangles.Length;
           
            for (int i = 0; i < cant; i++)
            {
                if (i == 0)
                {
                    r = RGB.r[0];
                    g = RGB.g[0];
                    b = RGB.b[0];
                }
                else if (i == 1)
                {
                    r = RGB.r[1];
                    g = RGB.g[1];
                    b = RGB.b[1];
                }
                else if (i == 2)
                {
                    r = RGB.r[2];
                    g = RGB.g[2];
                    b = RGB.b[2];
                }
                else if (i == 3)
                {
                    r = RGB.r[3];
                    g = RGB.g[3];
                    b = RGB.b[3];
                }
                else if (i == 4)
                {
                    r = RGB.r[4];
                    g = RGB.g[4];
                    b = RGB.b[4];
                }
                else if (i == 5)
                {
                    r = RGB.r[5];
                    g = RGB.g[5];
                    b = RGB.b[5];
                }
                else
                {
                    r = 0;
                    g = 0;
                    b = 0;
                }

                using (Graphics graphics = Graphics.FromImage(Imagen))
                {

                    using (Pen pen = new Pen(Color.FromArgb(r, g, b), 5))
                    {
                        graphics.DrawRectangle(pen, rectangles[i]);
                    }
                }
            }

            PB_TFoto.Image = Imagen;
        }

        private void TomarFoto_FormClosed(object sender, FormClosedEventArgs e)
        {
            CerrarCamara();
        }

        private void btn_Detener_Click(object sender, EventArgs e)
        {
            CerrarCamara();
        }

        private void btn_TomarFoto_Click(object sender, EventArgs e)
        {
            
            if (MiCamara != null && MiCamara.IsRunning)
            {
                string num = Convert.ToString(cant);
                Label_Cantidad.Text = "Rostros detectados en la foto: " + num;
                PB_ImSelecc.Image = copia;
            }
        }

        private void btn_SeleccFoto_Click(object sender, EventArgs e)
        {
            if (MiCamara != null && MiCamara.IsRunning)
            {
                Bitmap enviar = (Bitmap)PB_ImSelecc.Image;
                Form1.enviandoFoto = true;
                Form1.fotoEnviada = enviar;
                CerrarCamara();
                this.Close();
            }
        }
    }
}
