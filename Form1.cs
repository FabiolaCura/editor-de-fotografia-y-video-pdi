﻿using Procesamiento_de_imagenes.Cambios;
using System;
using System.Windows.Forms;
using System.Drawing;

namespace Procesamiento_de_imagenes
{
    public partial class Form1 : Form
    {
        static public bool CapturandoDispositivo = false;
        static public bool enviandoFoto = false;
        static public Bitmap fotoEnviada;
        public Form1()
        {
            InitializeComponent();
            AbrirFrom(new EditarFotografia());
        }

        #region MoverVentana

        int posY = 0;
        int posX = 0;

        private void panelControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            {
                posX = e.X;
                posY = e.Y;
            }
            else
            {
                Left = Left + (e.X - posX);
                Top = Top + (e.Y - posY);
            }

        }

        #endregion
        #region Cerrar_Minimizar

            private void btnCerrar_Click(object sender, EventArgs e)
            {
                Application.Exit();
            }

            private void btnMinimizar_Click(object sender, EventArgs e)
            {
                this.WindowState = FormWindowState.Minimized;
            }
        #endregion

        private void AbrirFrom(object FromAbrir)
        {
            if (this.panelCambio.Controls.Count > 0)
            {
                this.panelCambio.Controls.RemoveAt(0);
            }
            Form FCambio = FromAbrir as Form;
            //es un formulario secundario
            FCambio.TopLevel = false;
            FCambio.Dock = DockStyle.Fill;
            this.panelCambio.Controls.Add(FCambio);
            //contenedor de datos del panel
            this.panelCambio.Tag = FCambio;
            FCambio.Show();
            FCambio.FormClosed += Logout;
        
        }
        private void Logout(object sender, FormClosedEventArgs e)
        {
            if (this.panelCambio.Controls.Count > 0)
            {
                this.panelCambio.Controls.RemoveAt(0);
            }
            Form FP = new EditarFotografia();
            FP.TopLevel = false;
            FP.Dock = DockStyle.Fill;
            this.panelCambio.Controls.Add(FP);
            this.panelCambio.Tag = FP;
            FP.Show();

        }

        private void btnTomFot_Click(object sender, EventArgs e)
        {
            if (EditarFotografia.Im == false)
            {
                DialogResult resultado = new DialogResult();
                Form mensaje = new MessageBoxForm();
                resultado = mensaje.ShowDialog();
                if (resultado == DialogResult.Yes)
                {
                    EditarFotografia.Im = true;
                    AbrirFrom(new TomarFoto());
                }
            }
            else
            {
                if(CapturandoDispositivo == true)
                {
                    DialogResult resultado = new DialogResult();
                    Form mensaje = new MessageBoxCamara();
                    resultado = mensaje.ShowDialog();
                }
                else
                    AbrirFrom(new TomarFoto());
            }            
        }

        private void btnEditFoto_Click(object sender, EventArgs e)
        {

            if (EditarFotografia.Im == false)
            {
                DialogResult resultado = new DialogResult();
                Form mensaje = new MessageBoxForm();
                resultado = mensaje.ShowDialog();
                if (resultado == DialogResult.Yes)
                {
                    EditarFotografia.Im = true;
                    AbrirFrom(new EditarFotografia());
                }
            }
            else
            {
                if (CapturandoDispositivo == true)
                {
                    DialogResult resultado = new DialogResult();
                    Form mensaje = new MessageBoxCamara();
                    resultado = mensaje.ShowDialog();
                }
                else
                    AbrirFrom(new EditarFotografia());
            }           
        }

        private void btnEditVid_Click(object sender, EventArgs e)
        {
            if (EditarFotografia.Im == false)
            {
                DialogResult resultado = new DialogResult();
                Form mensaje = new MessageBoxForm();
                resultado = mensaje.ShowDialog();
                if (resultado == DialogResult.Yes)
                {
                    EditarFotografia.Im = true;
                    AbrirFrom(new EditarVideo());
                }
            }
            else
            {
                if (CapturandoDispositivo == true)
                {
                    DialogResult resultado = new DialogResult();
                    Form mensaje = new MessageBoxCamara();
                    resultado = mensaje.ShowDialog();
                }
                else
                    AbrirFrom(new EditarVideo());
            }            
        }

        public void EnviandoFoto()
        {

        }
    }
}
